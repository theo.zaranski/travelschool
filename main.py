# !/usr/bin/env python
from tkinter import *
from tkinter.ttk import *
from classes.Bus import *

main_window = Tk()
main_window.title("Bus")
main_window.geometry("1000x700")


liste_bus = []

def CreationBus():
    bus = Bus(len(liste_bus),10)
    liste_bus.append(len(liste_bus)+1)
    affichage_bus = Button(main_window, text="Bus numéro : " + str(bus.getId()), command=lambda: BusWindow(bus))
    affichage_bus.grid(column=1, row=int(bus.getId()) + 3)


def BusWindow(bus):
    bus_window = Toplevel(main_window) # Affiche une fenetre bus_window par dessus la fenetre main_window
    bus_window.title("Détails du BUS")
    bus_window.geometry("400x600")

    Label(bus_window, text="AJOUTER UN PASSAGER", padding=30).pack()

    Label(bus_window, text="Nom de la personne :").pack()
    first_name = Entry(bus_window)
    first_name.pack()

    Label(bus_window, text="Prénom de la personne :").pack()
    last_name = Entry(bus_window)
    last_name.pack()


    button_check_prof = BooleanVar()
    button_check_prof.set(False)
    Checkbutton(bus_window, variable=button_check_prof, command=lambda: ProfChecked(bus_window), text="Prof").pack()
    button_check_eleve = BooleanVar()
    button_check_eleve.set(False)
    Checkbutton(bus_window, variable=button_check_eleve, command=lambda: EleveChecked(bus_window), text="Eleve").pack()
    button_add_passenger = Button(bus_window, text="Ajouter",command=lambda: bus.addPassenger(first_name.get() + ' ' + last_name.get())).pack()


def ProfChecked(bus_window):
    button_is_referent = BooleanVar()
    button_is_referent.set(False)
    Checkbutton(bus_window, variable=button_is_referent, command=lambda: EleveChecked(bus_window), text="Est référent").pack()
def EleveChecked(bus_window):
    print("eleve")



# AFFICHAGE
Button(main_window, text="Ajouter un BUS", command=CreationBus).grid(row=0, column=0)




# la fenêtre s'affiche puis attend les interactions de l'usager
main_window.mainloop()

