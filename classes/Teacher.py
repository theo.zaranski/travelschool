from classes.Person import Person


class Teacher(Person):
    def __init__(self, is_referent):
        Person.__init__(self)
        self.is_referent = bool(is_referent)

    def isReferent(self, is_referent):
        return self.is_referent



