from classes.Person import Person


class Student(Person):
    def __init__(self, first_name, last_name, id_grade, is_present):
        super().__init__(first_name, last_name)
        self.id_grade = id_grade
        self.is_present = bool(is_present)

    def isPresent(self, is_present):
        return is_present

    def setPresent(self, is_present):
        is_present = True

    def getClass(self, id_grade):
        return id_grade

