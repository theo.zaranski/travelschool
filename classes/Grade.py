class Grade:
    def __init__(self, id, nb_max_students, students):
        self.id = id
        self.nb_max_students = nb_max_students
        self.students = students[:nb_max_students -1]

        # Ajout d'un étudiant avec vérification
        # du nombre max < à la liste

    def addStudent(self, student):
        if len(self.students) <= self.nb_max_students:
            self.students.append(student)
        else:
            print('ERROR, Maximum d\'étudiant atteint dans la classe.')
