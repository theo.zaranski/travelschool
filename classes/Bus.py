from classes.Person import Person
from classes.Student import Student
from classes.Teacher import Teacher



class Bus:
    passengers = []

    def __init__(self, id_bus, passenger_max):
        self.id_bus = id_bus
        self.passenger_max = 4 # A potentiellement modifier plus tard


    def createStudent(self, passenger):
        firstname, lastname = passenger.split(' ');
        return Student(firstname, lastname, 1, False)
        
    def addPassenger(self, passenger):
        passenger = self.createStudent(passenger)
        if len(self.passengers) >= self.passenger_max:
            print("Bus complet")
        else:
            self.passengers.append(passenger)

    # def addPassenger(self, passenger):
    #     if int(len(self.passengers)) >= int(self.passenger_max):
    #         print("Bus complet")
    #
    #     # On vérifie que passager est bien un prof
    #     if isinstance(passenger, Teacher):
    #         # Si il n'est
    #         if not self.checkIfReferent:
    #             self.passengers.append(passenger)
    #         else:
    #             print("Il y a déja un référent")
    #
    #     # On vérifie que le passager est bien un Student
    #     if isinstance(passenger, Teacher):
    #         Student(Person.Person, Student)
    #         # On vérifie qu'il n'y ai pas le nombre max de classes dans le bus, si c'est ok,
    #         # on ajoute un passenger à la liste passengers
    #         if isinstance(self.checkMaxGradeInCar(Student.getGrade())):
    #             self.passengers.append(passenger)
    #         else:
    #             print("Erreur car nombre max de classes dans le bus a été atteint")

    def checkMaxGradeInCar(self, classeToCheck, passengers):
        # Initialisation d'un set, et non d'une liste, cela permet de parcourir la liste et d'écarter
        # les doublons afin de récuperer uniquement les id de classes pour pas dépasser le Max (3)
        list_grade = {}
        for passenger in self.passengers:
            if isinstance(passenger, Student):
                list_grade.add(passenger.Grade.id)

        if len(list_grade) > 3:
            print("Trop de classe dans ce bus")
            return True

    def checkIfReferent(self, passengers):
        for passenger in passengers:
            if isinstance(passenger, Teacher):
                Teacher(Person.Person, Teacher)
            if Teacher.isReferent():
                return True
            return False

    def canLeave(self, passengers):

        possibility_to_leave = False
        all_students_present = False
        count_teacher = 0
        count_student = 0

        # Pour chaque eleve dans la liste des passagers
        for passenger in passengers:
            # Si un des eleve est un Prof, l'ajouter au compteur de prof
            if isinstance(passenger, Teacher):
                count_teacher = count_teacher + 1

            elif isinstance(passenger, Student):
                count_student = count_student + 1

                # On vérifie si un seul eleve est manquant
                if all_students_present and not passenger.isPresent():
                    all_students_present = False

        if passengers.__sizeof__() != 0 & count_teacher * 10 <= count_student:
            possibility_to_leave = True

        return possibility_to_leave and all_students_present

    def getId(self):
        return self.id_bus
