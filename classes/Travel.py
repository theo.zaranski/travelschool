
class Travel:
    def __init__(self, destination, weather, bus_lst, grades):
        self.destination = destination
        self.weather = weather
        self.bus = [bus_lst]
        self.grade = [grades]

        # Feature 5 :On cherche à savoir si un seul car ne peux pas partir
        # dans ce cas pas de départ pour les autres
    def allCarCanGo(self, bus_lst):
        for Bus in bus_lst:
            if not Bus.canLeave:
                break
        else:
            return True

    # feature 3
    def addPassengerToBus(self, id_bus, passenger, bus_lst):
        for Bus in bus_lst:
            if Bus.getId():
                Bus.addPassenger(passenger)

